package com.wior.math;

import com.google.common.math.BigIntegerMath;

public class MathLib {

    private MathLib() {
    }

    public static double pow(double number, double power) {
        return Math.pow(number, power);
    }

    public static int factorial(int number) {
        return BigIntegerMath.factorial(number).intValue();
    }

    public static double add(double first, double second) {
        return (first + second);
    }
}
